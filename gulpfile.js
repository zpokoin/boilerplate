//initialize all of our variables

var themeWatch = 'app/theme/**';
var imageWatch = 'app/assets/images/**';
var htmlWatch = 'app/*.html';
var initWatch = 'app/theme/init.scss';

const {src, dest, task, watch, series, parallel} = require('gulp');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var sourceMaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync');
var autoprefixer = require('gulp-autoprefixer');
var plumber = require('gulp-plumber');
var svgSprite = require('gulp-svg-sprite');
var cleanCSS = require('gulp-clean-css');
var fileinclude = require('gulp-file-include');
var cheerio = require('gulp-cheerio');
var replace = require('gulp-replace');

function fileInclude() {
    return src(['app/**/*.html'])
        .pipe(fileinclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(dest('./public'));
}

function svgIcons() {
    // More complex configuration example
    var config = {
        shape: {
            dimension: { // Set maximum dimensions
                maxWidth: 21,
                maxHeight: 21
            },
            spacing: { // Add padding
                padding: 0
            },
            // dest: 'out/intermediate-svg' // Keep the intermediate files
        },
        mode: {
            symbol: {
                sprite: 'icons.svg',
                dest: 'sprite',
                prefix: '.i--%s',
                example: true
            }
        },
        svg: {
            xmlDeclaration: false, // strip out the XML attribute
            doctypeDeclaration: false, // don't include the !DOCTYPE declaration
            namespaceClassnames: false
        }
    };
    return src('**/*.svg', {cwd: 'app/assets/icons'})
        // remove all fill and style declarations in out shapes
        .pipe(cheerio({
            run: function ($) {
                $('[style]').removeAttr('style');
                $('[class]').removeAttr('class');
            },
            parserOptions: { xmlMode: true }
        }))
        // cheerio plugin create unnecessary string '&gt;', so replace it.
        .pipe(replace('&gt;', '>'))
        .pipe(svgSprite(config))
        .pipe(dest('app/assets/'))
}

function cssMinify() {
    return src('app/styles/*.css')
        .pipe(cleanCSS())
        .pipe(dest('app/styles/minified'));
}

function browser_sync(done) {
    browserSync({
        server: {
            baseDir: "app/",
            middleware: function (req, res, next) {
                res.setHeader('Access-Control-Allow-Origin', '*');
                next();
            },
        },
        options: {
            reloadDelay: 250
        },
        notify: false
    });

    done();
}

//compiling Lia SCSS files
function cssInit() {
    return src('app/theme/themes/*.scss')
        .pipe(plumber({
            errorHandler: function (err) {
                console.log(err);
                this.emit('end');
            }
        }))
        .pipe(sourceMaps.init())
        .pipe(sass({
            errLogToConsole: true,
            includePaths: [
                'app/theme/'
            ]
        }))
        .pipe(autoprefixer({
            cascade: true,
            grid: false
        }))
        .pipe(sourceMaps.write())
        .pipe(dest('app/styles'))
        .pipe(browserSync.reload({stream: true}));
}

//compiling our SCSS files for deployment
function cssDeploy() {
    //the initializer / master SCSS file, which will just be a file that imports everything
    return src(initWatch)
        .pipe(plumber())
        .pipe(sass({
            includePaths: [
                'app/theme',
            ]
        }))
        .pipe(autoprefixer({
            cascade: true
        }))
        .pipe(concat('styles.css'))
        .pipe(cleanCSS())
        .pipe(dest('dist/css'));
}

function htmlSync() {
    return src(htmlWatch)
        .pipe(plumber())
        .pipe(browserSync.reload({stream: true}))
}

const copyAssets = () => {
    return src('app/assets/**')
        .pipe(dest('public/assets/'));
};

const copyStyles = () => {
    return src('app/styles/**')
        .pipe(dest('public/styles/'));
};

const watcher = () => {
    //a list of watchers, so it will watch all of the following files waiting for changes
    watch(themeWatch, cssInit());
    watch(themeWatch, cssMinify);
    watch(themeWatch, copyStyles);
    watch(imageWatch, svgIcons);
    watch(themeWatch, fileInclude);
};


task("default", series(fileInclude, cssInit, copyStyles, copyAssets, watcher));
task("watch", parallel(fileInclude, watcher, browser_sync));
task("deploy", series(copyAssets, copyStyles, fileInclude));
task("svg", svgIcons);